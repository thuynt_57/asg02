package oop.asg02;

public class BigInteger
{ 
    String a;
    public BigInteger(int init) {
        a=""+init;
        
    }

    public BigInteger(String init) {
        a=init;
    }

    public String toString() {
        long temp;
        temp = Long.valueOf(a);
        return ""+temp;
    }

    public boolean equals(Object other) {
        BigInteger b = (BigInteger) other;
        return a.toString().equals(b.toString());
    }

    public long toLong() {
        long temp;
        temp = Long.valueOf(a);
        return temp;  
    }
   
   public BigInteger add(BigInteger other) {
        int l1=a.length();
        int l2=(other.a).length();
        int intArray_a[]=new int[100];
        int intArray_other[]=new int[100];
        int intArray_sum[]=new int[100];
        int count=0;
        String str=new String();
        
        for(int i=0;i<l1;i++)
            intArray_a[i]=Integer.valueOf(a.charAt(i));
        for(int i=0;i<l2;i++)
            intArray_other[i]=Integer.valueOf((other.a).charAt(i));
        
        while(l1>0&&l2>0){
            if((intArray_a[l1]+intArray_other[l2])<10){
                intArray_sum[count++] = intArray_a[l1]+intArray_other[l2];
                l1--;
                l2--;
             }
            else{
                intArray_sum[count++] = (intArray_a[l1]+intArray_other[l2])-10;
                l1--;
                //carry_on=1;
                intArray_a[l1]++;
                l2--;
            }
        }
        while(l1>0){
            intArray_sum[count++]=intArray_a[l1];
            l1--;
        }
         while(l2>0){
            intArray_sum[count++]=intArray_a[l2];
            l2--;
        }
        
        String strArray[]=new String[100];
        for(int i=count-1;i>=0;i--){
            strArray[i]="" + intArray_sum[i];
            str+=strArray[i];
        }
        
        BigInteger sum = new BigInteger(str);
        
        return sum; 
    }

    public BigInteger subtract(BigInteger other) {
        int l1=a.length();
        int l2=(other.a).length();
        int intArray_a[]=new int[100];
        int intArray_other[]=new int[100];
        int intArray_sub[]=new int[100];
        int count=0;
        String str=new String();
        
        for(int i=0;i<l1;i++)
            intArray_a[i]=Integer.valueOf(a.charAt(i));
        for(int i=0;i<l2;i++)
            intArray_other[i]=Integer.valueOf((other.a).charAt(i));
        
        while(l1>0&&l2>0){
            if(intArray_a[l1]>=intArray_other[l2]){ 
                intArray_sub[count++] = intArray_a[l1]-intArray_other[l2];
                l1--;
                l2--;
             }
            else{
                intArray_sub[count++] = (10+intArray_a[l1])-intArray_other[l2];
                l1--;
                intArray_a[l1]--;
                l2--;
            }
        }
         while(l1>0){
            intArray_sub[count++]=intArray_a[l1];
            l1--;
        }
        
        String strArray[]=new String[100];
        for(int i=count-1;i>=0;i--){
            strArray[i]="" + intArray_sub[i];
            str+=strArray[i];
        }
        
        BigInteger sub = new BigInteger(str);
        
        return sub;  
    }

    public int compareTo(BigInteger other){
        int l1=a.length();
        int l2=(other.a).length();
        int intArray_a[]=new int[100];
        int intArray_other[]=new int[100];
        
        for(int i=0;i<l1;i++)
            intArray_a[i]=Integer.valueOf(a.charAt(i));
        for(int i=0;i<l2;i++)
            intArray_other[i]=Integer.valueOf((other.a).charAt(i));
        if(l1>l2) return 1;
        else
            if(l1<l2)  return -1;
            else{
                 for(int index=0;index<l1;index++){
                     if(intArray_a[index]>intArray_other[index]) return 1;
                     else
                         if(intArray_a[index]<intArray_other[index]) return -1;
                 }
                 return 0;
                     
            } 
    }
    
    public BigInteger clon(){
        BigInteger this_clon = new BigInteger(a);
        return this_clon;
    }
}